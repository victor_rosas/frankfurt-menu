//
//  CellComponent.h
//  MDRR
//
//  Created by Jesus Ruiz on 3/31/15.
//  Copyright (c) 2015 ItesmiOS. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface CellComponent : NSObject{
    
    
}

@property (nonatomic) NSString *Name;
@property (nonatomic) NSString *Description;

@end
