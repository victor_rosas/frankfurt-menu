//
//  AddVC.m
//  MDRR
//
//  Created by Jesus Ruiz on 4/18/15.
//  Copyright (c) 2015 ItesmiOS. All rights reserved.
//

#import "AddVC.h"

@interface AddVC ()

@end

@implementation AddVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    imapicker = [[UIImagePickerController alloc]init];
    imapicker.delegate = self;
    cam = YES;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)saveToParse: (UIImageView *)imagen simbolo: (UIImageView *)simbol {
    NSData* dataIma = UIImagePNGRepresentation(imagen.image);
    //NSData* dataSim = UIImagePNGRepresentation(simbol.image);
    PFFile *imageFile = [PFFile fileWithName:@"Image.jpg" data:dataIma];
    //PFFile *simbolFile = [PFFile fileWithName:Nombre.text data:dataSim];
    
    //[simbolFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
      //  if (!error) {
    
    
    if (imagen.image == nil || [Nombre.text isEqual:@""])//|| [Descripcion.text isEqual:@""] )
    {
        NSLog(@"error");
    }else{
        
        
        UIAlertController * alert_exito=   [UIAlertController
                                            alertControllerWithTitle:@"Atención"
                                            message:@"Datos guardados correctamente."
                                            preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Ok"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [self.navigationController popToRootViewControllerAnimated:true];
                             }];
        [alert_exito addAction:ok];
        
    
            PFObject* newObject = [PFObject objectWithClassName:@"Cervezas"];
        //newObject[@"simbolo"] = simbolFile;
            newObject[@"imagen"] = imageFile;
            newObject[@"nombre"] = Nombre.text.uppercaseString;
            newObject[@"marca"] = Marca.text.uppercaseString;
        newObject[@"pais_origen"] = Pais.text.uppercaseString;
        newObject[@"presentacion"] = Presentacion.text.uppercaseString;
        newObject[@"porcentaje_alcohol"] = Porcentaje.text.uppercaseString;
        int nstock = [Stock.text integerValue];
        newObject[@"stock"] = @(nstock);
    
            [newObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (!error) {
                    NSLog(@"Saved");
                    [self presentViewController:alert_exito animated:YES completion:nil];
                }
                else{
                    // Error
                    NSLog(@"Error: %@ %@", error, [error userInfo]);
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Ocurrio un error,por favor revise los datos e intente de nuevo" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }];
    }
    
    
    
      //  }
    //}];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [[picker presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [[picker presentingViewController] dismissViewControllerAnimated:YES completion:nil];

    if ( cam == YES ) {
        picIma = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        [self squareImageWithImage:picIma scaledToSize:CGSizeMake(Imagen.frame.size.width, Imagen.frame.size.height) imageView:Imagen];
        
        //[Imagen setImage:picIma];
    }
   /* if ( cam == NO ) {
        picSim = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        [self squareImageWithImage:picSim scaledToSize:CGSizeMake(Simbolo.frame.size.width, Simbolo.frame.size.height) imageView:Simbolo];
        
        //[Simbolo setImage:picSim];
    }*/
    

}

- (UIImage *)squareImageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize imageView:(UIImageView*)view{
    double ratio;
    double delta;
    CGPoint offset;
    
    //make a new square size, that is the resized imaged width
    CGSize sz = CGSizeMake(newSize.width, newSize.width);
    
    //figure out if the picture is landscape or portrait, then
    //calculate scale factor and offset
    if (image.size.width > image.size.height) {
        ratio = newSize.width / image.size.width;
        delta = (ratio*image.size.width - ratio*image.size.height);
        offset = CGPointMake(delta/2, 0);
    } else {
        ratio = newSize.width / image.size.height;
        delta = (ratio*image.size.height - ratio*image.size.width);
        offset = CGPointMake(0, delta/2);
    }
    
    //make the final clipping rect based on the calculated values
    CGRect clipRect = CGRectMake(-offset.x, -offset.y,
                                 (ratio * image.size.width) + delta,
                                 (ratio * image.size.height) + delta);
    
    
    //start a new context, with scale factor 0.0 so retina displays get
    //high quality image
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(sz, YES, 0.0);
    } else {
        UIGraphicsBeginImageContext(sz);
    }
    UIRectClip(clipRect);
    [image drawInRect:clipRect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    view.image = newImage;
    
    return newImage;
}

- (IBAction)btnCamI:(id)sender {
    cam = YES;
    imapicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:imapicker animated:YES completion:nil];
}

- (IBAction)btnAlbumI:(id)sender {
    cam = YES;
    imapicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:imapicker animated:YES completion:nil];
}

- (IBAction)btnCamS:(id)sender {
    cam = NO;
    imapicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:imapicker animated:YES completion:nil];
}

- (IBAction)btnAlbumS:(id)sender {
    cam = NO;
    imapicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:imapicker animated:YES completion:nil];
}

- (IBAction)btnSave:(id)sender {
    [self saveToParse:Imagen simbolo:nil];
}


@end
