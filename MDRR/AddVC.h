//
//  AddVC.h
//  MDRR
//
//  Created by Jesus Ruiz on 4/18/15.
//  Copyright (c) 2015 ItesmiOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface AddVC : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate>{
    
    __weak IBOutlet UITextField *Nombre;
    
    __weak IBOutlet UITextField *Marca;
    
    __weak IBOutlet UITextField *Pais;
    
    __weak IBOutlet UIImageView *Imagen;
    
    __weak IBOutlet UITextField *Presentacion;
    
    __weak IBOutlet UITextField *Porcentaje;
    
    __weak IBOutlet UITextField *Stock;
    
    UIImagePickerController *imapicker;
    
    UIImage *picIma, *picSim;
    
    BOOL cam;

}
- (IBAction)btnCamI:(id)sender;

- (IBAction)btnAlbumI:(id)sender;

- (IBAction)btnCamS:(id)sender;

- (IBAction)btnAlbumS:(id)sender;

- (IBAction)btnSave:(id)sender;

@end
