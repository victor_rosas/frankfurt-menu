//
//  TViewController.m
//  MDRR
//
//  Created by Jesus Ruiz on 3/31/15.
//  Copyright (c) 2015 ItesmiOS. All rights reserved.
//

#import "TViewController.h"

@interface TViewController ()

@end

@implementation TViewController
@synthesize Components;
@synthesize res;



- (void)viewDidLoad {
    [super viewDidLoad];
    
    //[self testInternetConnection];
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor grayColor];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self
                            action:@selector(reload_meh)
                  forControlEvents:UIControlEventValueChanged];
    
    Components = [[NSMutableArray alloc] init];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"nbackground.jpg"]];

}

- (void)reload_meh
{
    // Reload table data
    if ([srchLook.text isEqualToString:@""]) {
        [res removeAllObjects];
    }
    
    [self.tableView reloadData];
    
    // End the refreshing
    if (self.refreshControl) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        self.refreshControl.attributedTitle = attributedTitle;
        
        [self.refreshControl endRefreshing];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [self testInternetConnection];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - Reachability

-(void)testInternetConnection{
    internetReachableFoo = [Reachability reachabilityWithHostName:@"www.google.com"];
    
    internetReachableFoo.reachableBlock = ^(Reachability *reach){
        
        dispatch_async( dispatch_get_main_queue(), ^{
            NSLog(@"Got Internet");
            [self removeFromLocal];
            [self loadFromParse];
            [self retrieveLocalData];
        
        });
    };
    
    internetReachableFoo.unreachableBlock = ^(Reachability *reach){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"No internet");
            [self retrieveLocalData];
        });
    };

    [internetReachableFoo startNotifier];
    

}



#pragma mark - Load Components

-(void)loadFromParse{
    
    PFQuery *query = [PFQuery queryWithClassName:@"Cervezas"];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if( !error ){
    
            [PFObject pinAllInBackground:objects];
            
        }
        else{
            NSLog(@"Error %@ %@", error, [error userInfo]);
        }
        
        
    }];
    
    
}

-(void)removeFromLocal{
    PFQuery *query = [PFQuery queryWithClassName:@"Cervezas"];
    [query fromLocalDatastore];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if( !error ){
            NSLog(@"%ld",[objects count]);
            [PFObject unpinAllInBackground:objects];
        }
        else{
            NSLog(@"Error %@ %@", error, [error userInfo]);
        }
        
        
    }];
    
    
}

-(void)retrieveLocalData{
    [Components removeAllObjects];
    
    PFQuery  *query = [PFQuery queryWithClassName:@"Cervezas"];
    [query fromLocalDatastore];
    
    [[query findObjectsInBackground] continueWithBlock:^id(BFTask *task) {
        if (task.error) {
            NSLog(@"Error: %@", task.error);
            return task;
        }
        for(PFObject *obj in task.result){
            [Components addObject:obj];
        }
        [self.tableView reloadData];
        
        NSLog(@"Retrieved %lu", (unsigned long)[task.result count]);
        return task;
    }];
    
}

#pragma mark - Search

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    Found = NO;
    
    [srchLook resignFirstResponder];

    
    
    res = [NSMutableArray arrayWithCapacity:[Components count]];
    
    
    NSMutableArray *results = [[NSMutableArray alloc] init];
    
    
    for(PFObject *name in Components)
    {
        if ([name[@"nombre"] rangeOfString:srchLook.text options:NSLiteralSearch|NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch].location != NSNotFound){
            [results addObject:name];
            Found = YES;
        }
        
    }
    if(Found==NO){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No existe el componente o introdujo un caracter no valido." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    
    res = results.mutableCopy;
    
    [self.tableView reloadData];
    
    
}




#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {    
    if ([res count] == 0) {
        return [Components count];
    } else {
        return [res count];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NewComponent *cell = [tableView dequeueReusableCellWithIdentifier:@"new cell" forIndexPath:indexPath];

    if (cell == nil) {
        cell = [[NewComponent alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"new cell"];
    }
    
    if ( [res count] == 0 ) {
        PFObject *obj = (PFObject *) [Components objectAtIndex:indexPath.row];
        cell.Name.text = obj[@"nombre"];
        
        PFFile *file = [obj objectForKey:@"imagen"];
        cell.Symbol.file = file;
        [cell.Symbol loadInBackground];
    }else{
        PFObject *obj = (PFObject *) [res objectAtIndex:indexPath.row];
        cell.Name.text = obj[@"nombre"];
        
        PFFile *file = [obj objectForKey:@"imagen"];
        cell.Symbol.file = file;
        [cell.Symbol loadInBackground];
    }
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ComponentVC *compnt = (ComponentVC *)[self.storyboard instantiateViewControllerWithIdentifier:@"component"];
    
    if ([res count] == 0) {
        PFObject *obj = (PFObject *) [Components objectAtIndex:indexPath.row];
        PFFile *file = [obj objectForKey:@"imagen"];
        
        compnt.nombre = obj[@"nombre"];
        compnt.marca = obj[@"marca"];
        compnt.pais = obj[@"pais_origen"];
        compnt.presentacion = obj[@"presentacion"];
        compnt.porcentaje = obj[@"porcentaje_alcohol"];
        compnt.Name = obj[@"nombre"];
        compnt.fileS = file;
        compnt.Descripion = obj[@"nombre"];
        compnt.fileC =file;
    }else{
        PFObject *obj = (PFObject *) [res objectAtIndex:indexPath.row];
        PFFile *file = [obj objectForKey:@"imagen"];
        compnt.nombre = obj[@"nombre"];
        compnt.marca = obj[@"marca"];
        compnt.pais = obj[@"pais_origen"];
        compnt.presentacion = obj[@"presentacion"];
        compnt.porcentaje = obj[@"porcentaje_alcohol"];
        compnt.Name = obj[@"nombre"];
        compnt.fileS = file;
        compnt.Descripion = obj[@"nombre"];
        compnt.fileC =file;
    }
    

    
    [self showViewController:compnt sender:nil];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90.0;
}




- (IBAction)adminView:(id)sender {
    
    AdminCtrlTV *adminvc = [self.storyboard instantiateViewControllerWithIdentifier:@"AdminCtrl"];
    [self.navigationController pushViewController:adminvc animated:YES];
    
    
    //Codigo del touch ID
     /*LAContext *context = [[LAContext alloc]init];
     NSError *error = nil;
     
     if([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]){
     [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
     localizedReason:@"Are you the device owner?"
     reply:^(BOOL success, NSError *error) {
     
     if (error) {
     dispatch_async (dispatch_get_main_queue(), ^{
     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
     message:@"Hubo un problema verificando tu identidad."
     delegate:nil
     cancelButtonTitle:@"Ok"
     otherButtonTitles:nil];
     [alert show];
     });
     return;
     }
     
     if (success) {
     dispatch_async (dispatch_get_main_queue(), ^{
     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Exito"
     message:@"Puede proseguir"
     delegate:nil
     cancelButtonTitle:@"Ok"
     otherButtonTitles:nil];
     [alert show];
     AdminCtrlTV *adminvc = [self.storyboard instantiateViewControllerWithIdentifier:@"AdminCtrl"];
     [self.navigationController pushViewController:adminvc animated:YES];
     
     
     });
     
     } else {
     dispatch_async (dispatch_get_main_queue(), ^{
     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
     message:@"No es el dueño del dispositivo"
     delegate:nil
     cancelButtonTitle:@"Ok"
     otherButtonTitles:nil];
     [alert show];
     });
     
     }
     
     }];
     
     }
     else{
     
     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
     message:@"Tu dispositivo no puede reconocer Touch ID"
     delegate:nil
     cancelButtonTitle:@"Ok"
     otherButtonTitles:nil];
     [alert show];
     
     }*/
    // Termina codigo de touch id
    

}


@end
