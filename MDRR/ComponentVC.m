//
//  ComponentVC.m
//  MDRR
//
//  Created by Jesus Ruiz on 4/1/15.
//  Copyright (c) 2015 ItesmiOS. All rights reserved.
//

#import "ComponentVC.h"

@interface ComponentVC ()

@end

@implementation ComponentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    Description.backgroundColor = [UIColor clearColor];
    [Description setTextColor:[UIColor whiteColor]];
    [Description setFont:[UIFont systemFontOfSize:30]];
    Description.text = self.Descripion;
    lnombre.text = self.nombre;
    lmarca.text = [NSString stringWithFormat:@"MARCA: %@",self.marca];
    lpais.text = [NSString stringWithFormat:@"PAIS: %@",self.pais];
    lpresentacion.text = [NSString stringWithFormat:@"PRESENTACION: %@",self.presentacion];
    lporcentaje.text = [NSString stringWithFormat:@"Por. Alcohol: %@",self.porcentaje];
    self.navigationItem.title = self.Name;
    
    Symbol.file = self.fileS;
    [Symbol loadInBackground];
    Component.file = self.fileC;
    [Component loadInBackground];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background1.png"]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
