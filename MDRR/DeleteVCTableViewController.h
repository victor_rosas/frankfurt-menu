//
//  DeleteVCTableViewController.h
//  MDRR
//
//  Created by Jesus Ruiz on 4/18/15.
//  Copyright (c) 2015 ItesmiOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "NewComponent.h"
#import "TViewController.h"

@interface DeleteVCTableViewController : UITableViewController{
    
    NSMutableArray *res;
    NSMutableArray *Components;
    
    __weak IBOutlet UISearchBar *srchLook;
    
    BOOL Found;
}

@end
