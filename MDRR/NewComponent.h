//
//  NewComponent.h
//  MDRR
//
//  Created by Jesus Ruiz on 3/31/15.
//  Copyright (c) 2015 ItesmiOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>

@interface NewComponent : UITableViewCell{
    
}

@property (weak, nonatomic) IBOutlet PFImageView *Symbol;


@property (weak, nonatomic) IBOutlet UILabel *Name;


@end
