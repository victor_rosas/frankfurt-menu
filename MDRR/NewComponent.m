//
//  NewComponent.m
//  MDRR
//
//  Created by Jesus Ruiz on 3/31/15.
//  Copyright (c) 2015 ItesmiOS. All rights reserved.
//

#import "NewComponent.h"

@implementation NewComponent

- (void)awakeFromNib {
    // Initialization code
    self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"nbackground.jpg"]];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
