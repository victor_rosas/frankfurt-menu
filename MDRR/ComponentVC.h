//
//  ComponentVC.h
//  MDRR
//
//  Created by Jesus Ruiz on 4/1/15.
//  Copyright (c) 2015 ItesmiOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ParseUI/ParseUI.h>

@interface ComponentVC : UIViewController{
    
    __weak IBOutlet UITextView *Description;
    __weak IBOutlet PFImageView *Symbol;
    __weak IBOutlet PFImageView *Component;
    __weak IBOutlet UILabel *lnombre;
    __weak IBOutlet UILabel *lmarca;
    __weak IBOutlet UILabel *lpais;
    __weak IBOutlet UILabel *lpresentacion;
    __weak IBOutlet UILabel *lporcentaje;
    

}

@property (nonatomic) NSString *Descripion;

@property (nonatomic) NSString *nombre;
@property (nonatomic) NSString *marca;
@property (nonatomic) NSString *pais;
@property (nonatomic) NSString *presentacion;
@property (nonatomic) NSString *porcentaje;

@property (nonatomic) NSString *Name;

@property (nonatomic) PFFile *fileS;

@property (nonatomic) PFFile *fileC;

@end
