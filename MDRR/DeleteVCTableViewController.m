//
//  DeleteVCTableViewController.m
//  MDRR
//
//  Created by Jesus Ruiz on 4/18/15.
//  Copyright (c) 2015 ItesmiOS. All rights reserved.
//

#import "DeleteVCTableViewController.h"

@interface DeleteVCTableViewController ()

@end

@implementation DeleteVCTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor blueColor];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self
                            action:@selector(reload_meh)
                  forControlEvents:UIControlEventValueChanged];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background1.png"]];
    Components = [[NSMutableArray alloc] init];

    [self retrieveLocalData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reload_meh
{
    // Reload table data
    if ([srchLook.text isEqualToString:@""]) {
        [res removeAllObjects];
    }
    
    [self.tableView reloadData];
    
    // End the refreshing
    if (self.refreshControl) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        self.refreshControl.attributedTitle = attributedTitle;
        
        [self.refreshControl endRefreshing];
    }
}




-(void)retrieveLocalData{
    [Components removeAllObjects];
    
    PFQuery  *query = [PFQuery queryWithClassName:@"Cervezas"];
    [query fromLocalDatastore];
    
    [[query findObjectsInBackground] continueWithBlock:^id(BFTask *task) {
        if (task.error) {
            NSLog(@"Error: %@", task.error);
            return task;
        }
        for(PFObject *obj in task.result){
            [Components addObject:obj];
        }
        [self.tableView reloadData];
        
        NSLog(@"Retrieved %lu", (unsigned long)[task.result count]);
        return task;
    }];
    
}

#pragma mark - Search

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    Found = NO;
    
    [srchLook resignFirstResponder];
    
    
    res = [NSMutableArray arrayWithCapacity:[Components count]];
    
    
    NSMutableArray *results = [[NSMutableArray alloc] init];
    
    
    for(PFObject *name in Components)
    {
        if ([name[@"nombre"] rangeOfString:srchLook.text options:NSLiteralSearch|NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch].location != NSNotFound){
            [results addObject:name];
            Found = YES;
        }
        
    }
    if(Found==NO){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No existe el componente o introdujo un caracter no valido." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    
    res = results.copy;
    
    [self.tableView reloadData];
    
}






#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([res count] == 0) {
        return [Components count];
    } else {
        return [res count];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        NewComponent *cell = [tableView dequeueReusableCellWithIdentifier:@"new cell" forIndexPath:indexPath];
        
        if (cell == nil) {
            cell = [[NewComponent alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"new cell"];
        }
        
        if ( [res count] == 0 ) {
            PFObject *obj = (PFObject *) [Components objectAtIndex:indexPath.row];
            cell.Name.text = obj[@"nombre"];
            PFFile *file = [obj objectForKey:@"imagen"];
            cell.Symbol.file = file;
            [cell.Symbol loadInBackground];
        }else{
            PFObject *obj = (PFObject *) [res objectAtIndex:indexPath.row];
            cell.Name.text = obj[@"nombre"];
            
            PFFile *file = [obj objectForKey:@"imagen"];
            cell.Symbol.file = file;
            [cell.Symbol loadInBackground];
        }

    
    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90.0;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {

        PFObject *object = (PFObject *) [Components objectAtIndex:indexPath.row];
        [object unpinInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if ( succeeded ) {
                //[self.tableView reloadData];
                UIAlertController * alert_exito=   [UIAlertController
                                                    alertControllerWithTitle:@"Atención"
                                                    message:@"Datos borrados correctamente.Este articulo ya no se mostrara en la proxima actualizacion,por favor espere a que se actualice la base de datos"
                                                    preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"Ok"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [self.navigationController popToRootViewControllerAnimated:true];
                                     }];
                [alert_exito addAction:ok];
                [self presentViewController:alert_exito animated:YES completion:nil];
            }
        }];
        [object deleteEventually];
        // Delete the row from the data source
        //[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];

        
    } else if(editingStyle == UITableViewCellEditingStyleInsert){

    }
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
