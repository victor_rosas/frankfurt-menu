//
//  TViewController.h
//  MDRR
//
//  Created by Jesus Ruiz on 3/31/15.
//  Copyright (c) 2015 ItesmiOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <Bolts/Bolts.h>
#import "NewComponent.h"
#import "Reachability.h"
#import "ComponentVC.h"
#import "AdminCtrlTV.h"
#import <LocalAuthentication/LocalAuthentication.h>
#import "MainViewController.h"




@interface TViewController : UITableViewController{
    
    Reachability *internetReachableFoo;
    BOOL Found;
    
    __weak IBOutlet UISearchBar *srchLook;
    
}

- (IBAction)adminView:(id)sender;
@property (strong, nonatomic) NSMutableArray *Components;
@property (strong, nonatomic) NSMutableArray *res;

@end
